/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.tester;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author LENOVO
 */
public class XOUnitTest {

    public XOUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckwinRow1_O_output_true() {
        char table[][] = {{'O', 'O', 'O'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char player = 'O';
        boolean result = XO.cheackWin(table, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckwinRow2_O_output_true() {
        char table[][] = {{'-', '-', '-'}, {'O', 'O', 'O'}, {'-', '-', '-'}};
        char player = 'O';
        boolean result = XO.cheackWin(table, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckwinRow3_O_output_true() {
        char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'O', 'O', 'O'}};
        char player = 'O';
        boolean result = XO.cheackWin(table, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckwinCol1_O_output_true() {
        char table[][] = {{'O', '-', '-'}, {'O', '-', '-'}, {'O', '-', '-'}};
        char player = 'O';
        boolean result = XO.cheackWin(table, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckwinCol2_O_output_true() {
        char table[][] = {{'-', 'O', '-'}, {'-', 'O', '-'}, {'-', 'O', '-'}};
        char player = 'O';
        boolean result = XO.cheackWin(table, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckwinCol3_O_output_true() {
        char table[][] = {{'-', '-', 'O'}, {'-', '-', 'O'}, {'-', '-', 'O'}};
        char player = 'O';
        boolean result = XO.cheackWin(table, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckwinRowDiagonil_O_output_true() {
        char table[][] = {{'O', '-', '-'}, {'-', 'O', '-'}, {'-', '-', 'O'}};
        char player = 'O';
        boolean result = XO.cheackWin(table, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckwinRowDiagonil2_O_output_true() {
        char table[][] = {{'-', '-', 'O'}, {'-', 'O', '-'}, {'O', '-', '-'}};
        char player = 'O';
        boolean result = XO.cheackWin(table, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckwinRow1_X_output_true() {
        char table[][] = {{'X', 'X', 'X'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char player = 'X';
        boolean result = XO.cheackWin(table, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckwinRow2_X_output_true() {
        char table[][] = {{'-', '-', '-'}, {'X', 'X', 'X'}, {'-', '-', '-'}};
        char player = 'X';
        boolean result = XO.cheackWin(table, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckwinRow3_X_output_true() {
        char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'X', 'X', 'X'}};
        char player = 'X';
        boolean result = XO.cheackWin(table, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckwinCol1_X_output_true() {
        char table[][] = {{'X', '-', '-'}, {'X', '-', '-'}, {'X', '-', '-'}};
        char player = 'X';
        boolean result = XO.cheackWin(table, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckwinCol2_X_output_true() {
        char table[][] = {{'-', 'X', '-'}, {'-', 'X', '-'}, {'-', 'X', '-'}};
        char player = 'X';
        boolean result = XO.cheackWin(table, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckwinCol3_X_output_true() {
        char table[][] = {{'-', '-', 'O'}, {'-', '-', 'X'}, {'-', '-', 'X'}};
        char player = 'X';
        boolean result = XO.cheackWin(table, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckwinRowDiagonil_X_output_true() {
        char table[][] = {{'X', '-', '-'}, {'-', 'X', '-'}, {'-', '-', 'X'}};
        char player = 'X';
        boolean result = XO.cheackWin(table, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckwinRowDiagonil2_X_output_true() {
        char table[][] = {{'-', '-', 'X'}, {'-', 'X', '-'}, {'X', '-', '-'}};
        char player = 'X';
        boolean result = XO.cheackWin(table, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckdraw_false(){
        char[][] table = XO.gettable();
        boolean result = XO.checkDraw(table);
        assertEquals(false,result);
    }
}
